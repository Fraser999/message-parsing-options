#include <cassert>
#include <chrono>
#include <cstdint>
#include <iostream>
#include <map>
#include <vector>

using namespace std::chrono;

int main() {
  std::vector<int> v;
  for (int i = 0; i < 100000000; ++i)
    v.emplace_back(i % 3);

  auto start_time = high_resolution_clock::now();

  uint64_t total0 = 0;

  for (const auto& i : v) {
    switch (i) {
      case 0:
        total0 += 4;
        break;
      case 1:
        total0 += 5;
        break;
      case 2:
        total0 += 6;
        break;
      default:
        std::terminate();
    }
  }

  auto interval = duration_cast<nanoseconds>(high_resolution_clock::now() - start_time).count();
  std::cout << "Time for " << v.size() << " 'switch's:        " << interval / 1000000
            << " ms.  Total - " << total0 << '\n';



  uint64_t total1 = 0;
  std::map<int, int> m;
  m.emplace(0, 4);
  m.emplace(1, 5);
  m.emplace(2, 6);

  start_time = high_resolution_clock::now();

  for (const auto& i : v)
    total1 += m.at(i);

  interval = duration_cast<nanoseconds>(high_resolution_clock::now() - start_time).count();
  assert(total0 == total1);
  std::cout << "Time for " << v.size() << " 'std::map::at's   " << interval / 1000000
            << " ms.  Total - " << total1 << '\n';
}