#include <chrono>
#include <boost/variant.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/string.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/boost_variant.hpp>

struct Zero {
  template <typename Archive>
  void serialize(Archive& ref_archive) {
    ref_archive(i_data, c_data, sz_data);
  }

  std::ostream& Print(std::ostream& out_stream) const {
    out_stream << "[\n\t" << i_data << "\n\t" << c_data << "\n\t" << sz_data << "\n]\n\n";
    return out_stream;
  }

  int i_data{};
  char c_data{'Z'};
  std::string sz_data = "Zero";
};

struct One {
  One() {
    for (int i = 0; i < 100; ++i) {
      vec_string.push_back(std::string{"Struct One: Data number "} + std::to_string(i));
    }
  }

  std::ostream& Print(std::ostream& out_stream) const {
    out_stream << "[\n\t" << i_data << "\n\t" << c_data << "\n\t" << sz_data
               << "\n\t[ //begin: vector<string>";
    for (const auto& data : vec_string) {
      out_stream << "\n\t\t" << data;
    }
    out_stream << "\n\t] //end: vector<string>\n]\n\n";
    return out_stream;
  }

  template <typename Archive>
  void serialize(Archive& ref_archive) {
    ref_archive(i_data, c_data, sz_data, vec_string);
  }

  int i_data{1};
  char c_data{'O'};
  std::string sz_data = "One";
  std::vector<std::string> vec_string;
};

struct Two {
  Two() : vec_custom_data(1, CustomData(20)) { vec_custom_data.emplace_back(80); }
  template <typename Archive>
  void serialize(Archive& ref_archive) {
    ref_archive(i_data, c_data, sz_data, vec_custom_data);
  }

  std::ostream& Print(std::ostream& out_stream) const {
    out_stream << "[\n\t" << i_data << "\n\t" << c_data << "\n\t" << sz_data
               << "\n\t[ //begin: vector<CustomData>";
    for (const auto& data : vec_custom_data) {
      data.Print(out_stream);
    }
    out_stream << "\n\t] //end: vector<CustomData>\n]\n\n";
    return out_stream;
  }

  struct CustomData {
    CustomData(const int size = 1) {
      for (int i = 0; i < size; ++i) {
        vec_string.push_back(std::string{"Struct Two::CustomData: Data number "} +
                             std::to_string(i));
      }
    }

    template <typename Archive>
    void serialize(Archive& ref_archive) {
      ref_archive(vec_string);
    }

    std::ostream& Print(std::ostream& out_stream) const {
      out_stream << "\n\t\t[ //begin: vector<string>";
      for (const auto& data : vec_string) {
        out_stream << "\n\t\t\t" << data;
      }
      out_stream << "\n\t\t] //end: vector<string>";
      return out_stream;
    }

    std::vector<std::string> vec_string;
  };

  int i_data{2};
  char c_data{'T'};
  std::string sz_data = "Two";
  std::vector<CustomData> vec_custom_data;
};

template <typename TypeToSerialize>
std::string ConvertToString(TypeToSerialize&& obj_to_serialize) {
  std::stringstream string_stream;

  {
    cereal::BinaryOutputArchive output_bin_archive{string_stream};
    output_bin_archive(std::forward<TypeToSerialize>(obj_to_serialize));
  }

  return string_stream.str();
}

boost::variant<Zero, One, Two> variant_for_custom_types;

class Visitor : public boost::static_visitor<> {
 public:
  void operator()(const Zero& obj) const {
#ifdef OUTPUT_DESERIALIZED_DATA
    obj.Print(std::cerr);
#endif
  }
  void operator()(const One& obj) const {
#ifdef OUTPUT_DESERIALIZED_DATA
    obj.Print(std::cerr);
#endif
  }
  void operator()(const Two& obj) const {
#ifdef OUTPUT_DESERIALIZED_DATA
    obj.Print(std::cerr);
#endif
  }
} visitor;

void DeSerialize(std::stringstream& ref_binary_stream) {
  {
    cereal::BinaryInputArchive input_bin_archive{ref_binary_stream};
    input_bin_archive(variant_for_custom_types);
  }

  boost::apply_visitor(visitor, variant_for_custom_types);
}

int main() {
  std::vector<std::string> vec_binary_serialized_strings;

  for (int i = 0; i < 100000; ++i) {
    switch (i % 3) {
      case 0:
        variant_for_custom_types = Zero{};
        break;
      case 1:
        variant_for_custom_types = One{};
        break;
      case 2:
        variant_for_custom_types = Two{};
        break;
      default:
        std::terminate();
    }
    vec_binary_serialized_strings.push_back(ConvertToString(variant_for_custom_types));
  }

  std::stringstream string_stream;

  auto start_time = std::chrono::high_resolution_clock::now();

  for (const auto& data : vec_binary_serialized_strings) {
    string_stream.clear();
    string_stream.str(data);

    DeSerialize(string_stream);
  }

  auto end_time = std::chrono::high_resolution_clock::now();

  auto interval =
      std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time).count();
  std::cerr << "Execution time for boost::variant example for "
            << vec_binary_serialized_strings.size() << " binary de-serializations: " << interval
            << " ns"
            << " == " << interval / 1000000 << " ms." << std::endl;

  return 0;
}
