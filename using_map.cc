#include <map>
#include <chrono>
#include <cereal/types/vector.hpp>
#include <cereal/types/string.hpp>
#include <cereal/archives/binary.hpp>

enum class TypeTag : unsigned char { Zero, One, Two };

struct Base {
  Base(const TypeTag type_tag) : e_TYPE_TAG{type_tag} {}
  virtual void DeSerialize(std::stringstream& /*ref_binary_stream*/) = 0;
  virtual std::ostream& Print(std::ostream& /*out_stream*/ = std::cerr) const = 0;

  const TypeTag e_TYPE_TAG;
};

struct Zero : Base {
  Zero() : Base{TypeTag::Zero} {}

  void DeSerialize(std::stringstream& ref_binary_stream) override {
    cereal::BinaryInputArchive input_bin_archive{ref_binary_stream};
    input_bin_archive(*this);
  }

  template <typename Archive>
  void serialize(Archive& ref_archive) {
    ref_archive(i_data, c_data, sz_data);
  }

  std::ostream& Print(std::ostream& out_stream) const override {
    out_stream << "[\n\t" << i_data << "\n\t" << c_data << "\n\t" << sz_data << "\n]\n\n";
    return out_stream;
  }

  int i_data{};
  char c_data{'Z'};
  std::string sz_data = "Zero";
};

struct One : Base {
  One() : Base{TypeTag::One} {
    for (int i = 0; i < 100; ++i) {
      vec_string.push_back(std::string{"Struct One: Data number "} + std::to_string(i));
    }
  }

  void DeSerialize(std::stringstream& ref_binary_stream) override {
    cereal::BinaryInputArchive input_bin_archive{ref_binary_stream};
    input_bin_archive(*this);
  }

  std::ostream& Print(std::ostream& out_stream) const override {
    out_stream << "[\n\t" << i_data << "\n\t" << c_data << "\n\t" << sz_data
               << "\n\t[ //begin: vector<string>";
    for (const auto& data : vec_string) {
      out_stream << "\n\t\t" << data;
    }
    out_stream << "\n\t] //end: vector<string>\n]\n\n";
    return out_stream;
  }

  template <typename Archive>
  void serialize(Archive& ref_archive) {
    ref_archive(i_data, c_data, sz_data, vec_string);
  }

  int i_data{1};
  char c_data{'O'};
  std::string sz_data = "One";
  std::vector<std::string> vec_string;

  const TypeTag e_TYPE_TAG{TypeTag::One};
};

struct Two : Base {
  Two() : Base{TypeTag::Two}, vec_custom_data(1, CustomData{20}) {
    vec_custom_data.emplace_back(80);
  }

  void DeSerialize(std::stringstream& ref_binary_stream) override {
    cereal::BinaryInputArchive input_bin_archive{ref_binary_stream};
    input_bin_archive(*this);
  }

  template <typename Archive>
  void serialize(Archive& ref_archive) {
    ref_archive(i_data, c_data, sz_data, vec_custom_data);
  }

  std::ostream& Print(std::ostream& out_stream) const override {
    out_stream << "[\n\t" << i_data << "\n\t" << c_data << "\n\t" << sz_data
               << "\n\t[ //begin: vector<CustomData>";
    for (const auto& data : vec_custom_data) {
      data.Print(out_stream);
    }
    out_stream << "\n\t] //end: vector<CustomData>\n]\n\n";
    return out_stream;
  }

  struct CustomData {
    CustomData(const int size = 1) {
      for (int i = 0; i < size; ++i) {
        vec_string.push_back(std::string{"Struct Two::CustomData: Data number "} +
                             std::to_string(i));
      }
    }

    template <typename Archive>
    void serialize(Archive& ref_archive) {
      ref_archive(vec_string);
    }

    std::ostream& Print(std::ostream& out_stream) const {
      out_stream << "\n\t\t[ //begin: vector<string>";
      for (const auto& data : vec_string) {
        out_stream << "\n\t\t\t" << data;
      }
      out_stream << "\n\t\t] //end: vector<string>";
      return out_stream;
    }

    std::vector<std::string> vec_string;
  };

  int i_data{2};
  char c_data{'T'};
  std::string sz_data = "Two";
  std::vector<CustomData> vec_custom_data;

  const TypeTag e_TYPE_TAG{TypeTag::Two};
};

//--------------------------------------------------------------------------------------
using Map_t = std::map<TypeTag, std::unique_ptr<Base>>;

struct MapImpl {
  Map_t map;

  MapImpl() {
    map.emplace(TypeTag::Zero, std::unique_ptr<Base>{new Zero});
    map.emplace(TypeTag::One, std::unique_ptr<Base>{new One});
    map.emplace(TypeTag::Two, std::unique_ptr<Base>{new Two});
  }

  auto at(const TypeTag type_tag) -> decltype(map.at(type_tag)) { return map.at(type_tag); }
} Map;

//--------------------------------------------------------------------------------------

template <typename TypeToSerialize>
std::string ConvertToString(TypeToSerialize&& obj_to_serialize) {
  std::stringstream string_stream;

  {
    cereal::BinaryOutputArchive output_bin_archive{string_stream};
    output_bin_archive(obj_to_serialize.e_TYPE_TAG,
                       std::forward<TypeToSerialize>(obj_to_serialize));
  }

  return string_stream.str();
}
//--------------------------------------------------------------------------------------

void DeSerialize(std::stringstream& ref_binary_stream) {
  unsigned char uc_tag;

  {
    cereal::BinaryInputArchive input_bin_archive{ref_binary_stream};
    input_bin_archive(uc_tag);
  }

  Map.at(static_cast<TypeTag>(uc_tag))->DeSerialize(ref_binary_stream);

#ifdef OUTPUT_DESERIALIZED_DATA
  Map.at(static_cast<TypeTag>(uc_tag))->Print();
#endif
}
//---------------------------------------------------------------------------------------

int main() {
  std::vector<std::string> vec_binary_serialized_strings;

  for (int i = 0; i < 100000; ++i) {
    switch (i % 3) {
      case 0:
        vec_binary_serialized_strings.push_back(ConvertToString(Zero{}));
        break;
      case 1:
        vec_binary_serialized_strings.push_back(ConvertToString(One{}));
        break;
      case 2:
        vec_binary_serialized_strings.push_back(ConvertToString(Two{}));
        break;
      default:
        std::terminate();
    }
  }

  std::stringstream string_stream;

  auto start_time = std::chrono::high_resolution_clock::now();

  for (const auto& data : vec_binary_serialized_strings) {
    string_stream.clear();
    string_stream.str(data);

    DeSerialize(string_stream);
  }

  auto end_time = std::chrono::high_resolution_clock::now();

  auto interval =
      std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time).count();
  std::cerr << "Execution time for std::map example for " << vec_binary_serialized_strings.size()
            << " binary de-serializations: " << interval << " ns"
            << " == " << interval / 1000000 << " ms." << std::endl;

  return 0;
}
