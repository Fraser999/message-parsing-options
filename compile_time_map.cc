#include <chrono>
#include <cereal/types/vector.hpp>
#include <cereal/types/string.hpp>
#include <cereal/archives/binary.hpp>

enum class TypeTag : unsigned char { Zero, One, Two };

//--------------------------------------------------------------------------------------
struct Zero;
struct One;
struct Two;

template <TypeTag Key, typename Value, typename NextNode>
struct CompileTimeMapper;
struct ERROR_given_tag_is_not_mapped_to_a_type;

template <TypeTag, typename>
struct Pair;

template <typename...>
struct GetMap;

template <TypeTag Key, typename Value, typename... Types>
struct GetMap<Pair<Key, Value>, Types...> {
  using MAP = CompileTimeMapper<Key, Value, typename GetMap<Types...>::MAP>;
};

template <TypeTag Key, typename Value>
struct GetMap<Pair<Key, Value>> {
  using MAP = CompileTimeMapper<Key, Value, ERROR_given_tag_is_not_mapped_to_a_type>;
};

template <typename, TypeTag>
struct Find;

template <TypeTag Key, typename Value, typename NextNode, TypeTag KeyToFind>
struct Find<CompileTimeMapper<Key, Value, NextNode>, KeyToFind> {
  using ResultCustomType = typename Find<NextNode, KeyToFind>::ResultCustomType;
};

template <TypeTag KeyToFind, typename Value, typename NextNode>
struct Find<CompileTimeMapper<KeyToFind, Value, NextNode>, KeyToFind> {
  using ResultCustomType = Value;
};

// You will go on populating new stuffs here...power of variadics :)
using MAP =
    GetMap<Pair<TypeTag::Zero, Zero>, Pair<TypeTag::One, One>, Pair<TypeTag::Two, Two>>::MAP;

template <TypeTag Key>
using CustomType = typename Find<MAP, Key>::ResultCustomType;

//--------------------------------------------------------------------------------------

struct Zero {
  template <typename Archive>
  void serialize(Archive& ref_archive) {
    ref_archive(i_data, c_data, sz_data);
  }

  std::ostream& Print(std::ostream& out_stream) const {
    out_stream << "[\n\t" << i_data << "\n\t" << c_data << "\n\t" << sz_data << "\n]\n\n";
    return out_stream;
  }

  int i_data{};
  char c_data{'Z'};
  std::string sz_data = "Zero";

  const TypeTag e_TYPE_TAG{TypeTag::Zero};
};

struct One {
  One() {
    for (int i = 0; i < 100; ++i) {
      vec_string.push_back(std::string{"Struct One: Data number "} + std::to_string(i));
    }
  }

  std::ostream& Print(std::ostream& out_stream) const {
    out_stream << "[\n\t" << i_data << "\n\t" << c_data << "\n\t" << sz_data
               << "\n\t[ //begin: vector<string>";
    for (const auto& data : vec_string) {
      out_stream << "\n\t\t" << data;
    }
    out_stream << "\n\t] //end: vector<string>\n]\n\n";
    return out_stream;
  }

  template <typename Archive>
  void serialize(Archive& ref_archive) {
    ref_archive(i_data, c_data, sz_data, vec_string);
  }

  int i_data{1};
  char c_data{'O'};
  std::string sz_data = "One";
  std::vector<std::string> vec_string;

  const TypeTag e_TYPE_TAG{TypeTag::One};
};

struct Two {
  Two() : vec_custom_data(1, CustomData(20)) { vec_custom_data.emplace_back(80); }

  template <typename Archive>
  void serialize(Archive& ref_archive) {
    ref_archive(i_data, c_data, sz_data, vec_custom_data);
  }

  std::ostream& Print(std::ostream& out_stream) const {
    out_stream << "[\n\t" << i_data << "\n\t" << c_data << "\n\t" << sz_data
               << "\n\t[ //begin: vector<CustomData>";
    for (const auto& data : vec_custom_data) {
      data.Print(out_stream);
    }
    out_stream << "\n\t] //end: vector<CustomData>\n]\n\n";
    return out_stream;
  }

  struct CustomData {
    CustomData(const int size = 1) {
      for (int i = 0; i < size; ++i) {
        vec_string.push_back(std::string{"Struct Two::CustomData: Data number "} +
                             std::to_string(i));
      }
    }

    template <typename Archive>
    void serialize(Archive& ref_archive) {
      ref_archive(vec_string);
    }

    std::ostream& Print(std::ostream& out_stream) const {
      out_stream << "\n\t\t[ //begin: vector<string>";
      for (const auto& data : vec_string) {
        out_stream << "\n\t\t\t" << data;
      }
      out_stream << "\n\t\t] //end: vector<string>";
      return out_stream;
    }

    std::vector<std::string> vec_string;
  };

  int i_data{2};
  char c_data{'T'};
  std::string sz_data = "Two";
  std::vector<CustomData> vec_custom_data;

  const TypeTag e_TYPE_TAG{TypeTag::Two};
};

template <typename TypeToSerialize>
std::string ConvertToString(TypeToSerialize&& obj_to_serialize) {
  std::stringstream string_stream;

  {
    cereal::BinaryOutputArchive output_bin_archive{string_stream};
    output_bin_archive(obj_to_serialize.e_TYPE_TAG,
                       std::forward<TypeToSerialize>(obj_to_serialize));
  }

  return string_stream.str();
}

TypeTag ExtractTypeFromStream(std::stringstream& ref_binary_stream) {
  unsigned char uc_tag;

  {
    cereal::BinaryInputArchive input_bin_archive{ref_binary_stream};
    input_bin_archive(uc_tag);
  }

  return static_cast<TypeTag>(uc_tag);
}

template <TypeTag eTypeTag>
CustomType<eTypeTag> DeSerializeStreamToType(std::stringstream& ref_binary_stream) {
  CustomType<eTypeTag> obj_deserialized;

  {
    cereal::BinaryInputArchive input_bin_archive{ref_binary_stream};
    input_bin_archive(obj_deserialized);
  }

  return obj_deserialized;
}

int main() {
  std::vector<std::string> vec_binary_serialized_strings;

  for (int i = 0; i < 100000; ++i) {
    switch (i % 3) {
      case 0:
        vec_binary_serialized_strings.push_back(ConvertToString(Zero{}));
        break;
      case 1:
        vec_binary_serialized_strings.push_back(ConvertToString(One{}));
        break;
      case 2:
        vec_binary_serialized_strings.push_back(ConvertToString(Two{}));
        break;
      default:
        std::terminate();
    }
  }

  std::stringstream string_stream;

  auto start_time = std::chrono::high_resolution_clock::now();

  for (const auto& data : vec_binary_serialized_strings) {
    string_stream.clear();
    string_stream.str(data);

    switch (ExtractTypeFromStream(string_stream)) {
      case TypeTag::Zero: {
        DeSerializeStreamToType<TypeTag::Zero>(string_stream)
#ifdef OUTPUT_DESERIALIZED_DATA
            .Print(std::cerr)
#endif
            ;
        break;
      }
      case TypeTag::One: {
        DeSerializeStreamToType<TypeTag::One>(string_stream)
#ifdef OUTPUT_DESERIALIZED_DATA
            .Print(std::cerr)
#endif
            ;
        break;
      }
      case TypeTag::Two: {
        DeSerializeStreamToType<TypeTag::Two>(string_stream)
#ifdef OUTPUT_DESERIALIZED_DATA
            .Print(std::cerr)
#endif
            ;
        break;
      }
      default:
        std::terminate();
    }
  }

  auto end_time = std::chrono::high_resolution_clock::now();

  auto interval =
      std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time).count();
  std::cerr << "Execution time for compile-time-map example for "
            << vec_binary_serialized_strings.size() << " binary de-serializations: " << interval
            << " ns"
            << " == " << interval / 1000000 << " ms." << std::endl;

  return 0;
}
